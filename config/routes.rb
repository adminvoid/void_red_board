# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html
#get 'redboard', to: 'redboard#index', :via => [:post]
#get 'redboard' => 'redboard#options', :via => [:options]
match 'redboard', to: 'redboard#index', via: [:get]
match 'redboard/view/:id', to: 'redboard#view', via: [:get], as: 'board_view'

match 'redboard/api/all', :controller => 'redboard_api', :action => 'index', :via => :get
match 'redboard/api/all', to: 'redboard_api#options', via: [:options]

match 'redboard/api/create', :controller => 'redboard_api', :action => 'create', :via => :post
match 'redboard/api/create', to: 'redboard_api#options', via: [:options]

match 'redboard/api/update/:id', :controller => 'redboard_api', :action => 'update', :via => :post
match 'redboard/api/update/:id', to: 'redboard_api#options', via: [:options]

match 'redboard/api/create-booking', :controller => 'redboard_api', :action => 'createBooking', :via => :post
match 'redboard/api/create-booking', to: 'redboard_api#options', via: [:options]