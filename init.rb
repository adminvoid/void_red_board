Redmine::Plugin.register :void_red_board do
  name 'Void Red Board plugin'
  author 'Hamza Bahlaouane'
  description 'Boards plugin for Redmine'
  version '0.0.1'
  url 'https://bitbucket.org/adminvoid/void-red-board'
  author_url 'https://void.fr/'
  menu :top_menu, :void_red_board, { controller: 'redboard', action: 'index' }, caption: 'Boards'
  settings default: {
    save_log:         false,
    issue_done_ratio: false
  }, partial:       'settings/void_red_board_settings'

end
