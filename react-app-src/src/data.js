import {colors} from '@atlaskit/theme';
import finnImg from './static/media/finn-min.png';
import bmoImg from './static/media/bmo-min.png';
import princessImg from './static/media/princess-min.png';
import jakeImg from './static/media/jake-min.png';

const jake = {
    id: '1',
    name: 'Jake',
    url: 'http://adventuretime.wikia.com/wiki/Jake',
    avatarUrl: jakeImg,
    colors: {
        soft: colors.Y50,
        hard: colors.N400A,
    },
};

const BMO = {
    id: '2',
    name: 'BMO',
    url: 'http://adventuretime.wikia.com/wiki/BMO',
    avatarUrl: bmoImg,
    colors: {
        soft: colors.G50,
        hard: colors.N400A,
    },
};

const finn = {
    id: '3',
    name: 'Finn',
    url: 'http://adventuretime.wikia.com/wiki/Finn',
    avatarUrl: finnImg,
    colors: {
        soft: colors.B50,
        hard: colors.N400A,
    },
};

const princess = {
    id: '4',
    name: 'Princess bubblegum',
    url: 'http://adventuretime.wikia.com/wiki/Princess_Bubblegum',
    avatarUrl: princessImg,
    colors: {
        soft: colors.P50,
        hard: colors.N400A,
    },
};

const John = {
    id: '5',
    name: 'John',
    url: 'http://adventuretime.wikia.com/wiki/Finn',
    avatarUrl: finnImg,
    colors: {
        soft: colors.B50,
        hard: colors.N400A,
    },
};

const Toto = {
    id: '6',
    name: 'John',
    url: 'http://adventuretime.wikia.com/wiki/Finn',
    avatarUrl: finnImg,
    colors: {
        soft: colors.B50,
        hard: colors.N400A,
    },
};

const Sam = {
    id: '7',
    name: 'Sam',
    url: 'http://adventuretime.wikia.com/wiki/Finn',
    avatarUrl: finnImg,
    colors: {
        soft: colors.B50,
        hard: colors.N400A,
    },
};

const July = {
    id: '8',
    name: 'July',
    url: 'http://adventuretime.wikia.com/wiki/Princess_Bubblegum',
    avatarUrl: princessImg,
    colors: {
        soft: colors.P50,
        hard: colors.N400A,
    },
};

export const authors = [jake, BMO, finn];

export const quotes = [
    {
        id: '1',
        subject: 'Issue #1',
        project: "DAM",
        status: 1,
        author: BMO,
    },
    {
        id: '2',
        subject: 'Issue #2',
        project: "DAM",
        status: 3,
        author: jake,
    },
    {
        id: '3',
        subject: 'Issue #3',
        project: "Vactory",
        status: 2,
        author: jake,
    },
    {
        id: '4',
        subject: 'Issue #4',
        project: "CDG",
        status: 4,
        author: finn,
    },
    {
        id: '5',
        subject: 'Issue #5',
        project: "Capital Aur",
        status: 1,
        author: finn,
    },
    {
        id: '6',
        subject: 'Issue #6',
        project: "Elsan",
        status: 1,
        author: princess,
    },
    {
        id: '7',
        subject: 'Issue #7',
        project: "VOID",
        status: 4,
        author: princess,
    },
    {
        id: '8',
        subject: 'Issue #8',
        project: "Atiijari Wafabank",
        status: 2,
        author: finn,
    },
    {
        id: '9',
        subject: 'Issue #9',
        project: "Saham",
        status: 1,
        author: finn,
    },
    {
        id: '10',
        subject: 'Issue #10',
        project: "Saham",
        status: 4,
        author: princess,
    },
    {
        id: '11',
        subject: 'Issue #11',
        project: "CE",
        status: 4,
        author: princess,
    },
    {
        id: '12',
        subject: 'Issue #12',
        project: "BFIG",
        status: 1,
        author: princess,
    },
];

// So we do not have any clashes with our hardcoded ones
let idCount = quotes.length + 1;

export const getQuotes = (count) =>
    Array.from({length: count}, (v, k) => k).map(() => {
        const random = quotes[Math.floor(Math.random() * quotes.length)];
        const custom = {
            ...random,
            id: `G${idCount++}`,
        };

        return custom;
    });

export const getAuthors = (count) =>
    Array.from({length: count}, (v, k) => k).map(() => {
        const random = authors[Math.floor(Math.random() * authors.length)];

        const custom = {
            ...random,
            id: `author-${idCount++}`,
        };

        return custom;
    });

const getByAuthor = (author, items) =>
    items.filter((quote) => quote.author === author);

export const authorQuoteMap = authors.reduce(
    (previous, author) => ({
        ...previous,
        [author.name]: getByAuthor(author, quotes),
    }),
    {},
);

export const generateQuoteMap = (quoteCount) =>
    authors.reduce(
        (previous, author) => ({
            ...previous,
            [author.name]: getQuotes(quoteCount / authors.length),
        }),
        {},
    );

export const rData = [
    {
        author: {
            id: 1,
            drop_id: 'author_1',
            name: 'Hamza Bahlaouane',
            avatarUrl: jakeImg,
        },
        issues: [
            {
                id: 1,
                drop_id: 'issue_1',
                subject: 'Issue #1',
                project: "DAM",
                status: 1,
            },
            {
                id: 2,
                drop_id: 'issue_2',
                subject: 'Issue #2',
                project: "VACTORY",
                status: 2,
            },
            {
                id: 3,
                drop_id: 'issue_3',
                subject: 'Issue #3',
                project: "V3",
                status: 3,
            },
        ]
    },
    {
        author: {
            id: 2,
            drop_id: 'author_2',
            name: 'Mehdi Najeddine',
            avatarUrl: bmoImg,
        },
        issues: [
            {
                id: 4,
                drop_id: 'issue_4',
                subject: 'Test test',
                project: "VOID",
                status: 1,
            }
        ]
    }
]
