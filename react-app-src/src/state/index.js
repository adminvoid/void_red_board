import React from 'react';
import {createContainer} from "unstated-next";
import {useMutation, useQueryCache} from "react-query";
import {postIdea, updateIdea, postBooking} from "../ideas-api";

function useIssues(initialState = []) {
    const cache = useQueryCache()
    let [issues, setIssues] = React.useState(initialState)

    const [addIssue] = useMutation(postIdea, {
        onSuccess: (res) => {
            let current = []
            if (typeof issues[res.assigned] !== 'undefined') {
                current = [...issues[res.assigned]];
            }
            current.push(res)
            setIssues({
                ...issues,
                [res.assigned]: current,
            })

            cache.invalidateQueries('ideas')
        },
    })

    const [addBooking] = useMutation(postBooking)

    const [moveIssue] = useMutation(({issue}) => updateIdea(issue), {
        onSuccess: (res, {issuesMap}) => {
            let issuesList = {...issuesMap}
            let current = []
            if (typeof issuesList[res.assigned] !== 'undefined') {
                current = [...issuesList[res.assigned]];
            }

            let filteredCurrent = current.map(issue => {
                if (issue.id === res.id) {
                    return res
                }
                return issue
            });

            setIssues({
                ...issuesList,
                [res.assigned]: filteredCurrent,
            })

            // Query Invalidations
            cache.invalidateQueries('ideas')
        },
    })


    const [updateIssue] = useMutation(updateIdea, {
        onSuccess: (res) => {
            let current = []
            if (typeof issues[res.assigned] !== 'undefined') {
                current = [...issues[res.assigned]];
            }

            let filteredCurrent = current.map(issue => {
                if (issue.id === res.id) {
                    return res
                }
                return issue
            });

            setIssues({
                ...issues,
                [res.assigned]: filteredCurrent,
            })

            // Query Invalidations
            cache.invalidateQueries('ideas')
        },
    })

    const [markIssueAsClose] = useMutation(updateIdea, {
        onSuccess: (res) => {
            let current = []
            if (typeof issues[res.assigned] !== 'undefined') {
                current = [...issues[res.assigned]];
            }

            let filteredCurrent = current.filter(function (value, index, arr) {
                return value.id !== res.id;
            });

            setIssues({
                ...issues,
                [res.assigned]: filteredCurrent,
            })

            // Query Invalidations
            cache.invalidateQueries('ideas')
        },
    })


    const [markIssueAsDone] = useMutation(updateIdea, {
        onSuccess: (res) => {
            // Query Invalidations
            cache.invalidateQueries('ideas')
        },
    })

    return {issues, addIssue, updateIssue, setIssues, markIssueAsClose, markIssueAsDone, moveIssue, addBooking}
}

let Issues = createContainer(useIssues)

export default Issues

const useAppConfig = (initialState = []) => {
    let [config, setConfig] = React.useState(initialState)
    return {config, setConfig}
}

export const AppConfig = createContainer(useAppConfig)
