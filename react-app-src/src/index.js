import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import { ReactQueryDevtools } from 'react-query-devtools'
import {QueryCache, ReactQueryCacheProvider} from "react-query";
const queryCache = new QueryCache()

ReactDOM.render(
    <React.StrictMode>
        {/*<ReactQueryDevtools />*/}
        <ReactQueryCacheProvider queryCache={queryCache}>
            <App/>
        </ReactQueryCacheProvider>
    </React.StrictMode>,
    document.getElementById('root')
);
