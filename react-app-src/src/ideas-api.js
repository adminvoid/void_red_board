export const redmineAPIKey = '1600249a51880a5a81c5b386b19df1a19d6ab72a'
let extraHeaders = {}
let endpointURL = "/redboard/api"

if (process.env.NODE_ENV !== 'production') {
    extraHeaders = {
        'X-Redmine-API-Key': redmineAPIKey
    }
    endpointURL = "http://localhost:4000/redboard/api"
}

export const getIdeas = () => {
    let url = endpointURL + "/all"
    if (typeof window.board_group_id !== 'undefined') {
        url += '?group=' + parseInt(window.board_group_id)
    }
    return fetch(
        url,
        {
            headers: {
                ...extraHeaders
            },
        }
    ).then((res) => res.json())
}

export const postIdea = (idea) => {
    return fetch(endpointURL + '/create', {
        method: 'post',
        body: JSON.stringify(idea),
        headers: {
            ...extraHeaders,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(res => res.json())
}

export const updateIdea = (idea) => {
    return fetch(`${endpointURL}/update/${idea.id}`, {
        method: 'post',
        body: JSON.stringify(idea),
        headers: {
            ...extraHeaders,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    }).then(res => res.json())
}

export const postBooking = (booking) => {
    console.log("API Send", booking)
    return fetch(endpointURL + '/create-booking', {
        method: 'post',
        body: JSON.stringify(booking),
        headers: {
            ...extraHeaders,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    })
}