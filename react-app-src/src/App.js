import React from 'react';
import IssuesState, {AppConfig} from './state'
import './App.css';
import Board from './components/board/Board';
import {useQuery, useQueryCache} from 'react-query'
import {getIdeas} from './ideas-api'
import {Spin, Alert} from 'antd';
import {LoadingOutlined} from '@ant-design/icons';

const antIcon = <LoadingOutlined style={{fontSize: 24}} spin/>;

function App() {
    const {isLoading, error, data, refetch} = useQuery("ideas", getIdeas);

    if (isLoading) return <Spin indicator={antIcon}/>;
    if (error) return <Alert
        message="Error"
        description={"An error has occurred: " + error.message}
        type="error"
        showIcon
    />;

    const {issues, ...rest} = data
    return (
        <div className="redBoardApp">
            <AppConfig.Provider initialState={rest}>
                <IssuesState.Provider initialState={issues}>
                    <Board refetch={refetch}/>
                </IssuesState.Provider>
            </AppConfig.Provider>
        </div>
    );
}

export default App;
