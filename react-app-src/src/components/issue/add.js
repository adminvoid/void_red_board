import React, { useState } from "react";
import {
  Drawer,
  Form,
  Button,
  Col,
  Row,
  Input,
  Select,
  DatePicker,
  message,
  InputNumber,
  Modal,
  Tooltip,
} from "antd";
import {
  PlusCircleOutlined,
  SendOutlined,
  InfoCircleOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import moment from "moment";
import AddButton from "../primatives/float-add-button";
import { DATEFORMAT } from "../constants";
import IssuesState, { AppConfig } from "../../state";

const { Option } = Select;

function AddIssue() {
  const [visible, setVisible] = useState(false);
  const [bookingDrawer, setBookingDrawer] = useState(false);
  const [showBookingForm, setShowBookingForm] = useState(false);
  const [savingBookingForm, setSavingBookingForm] = useState(false);
  const [canCreateBooking, setCanCreateBooking] = useState(false);
  let { addIssue, addBooking } = IssuesState.useContainer();
  let { config } = AppConfig.useContainer();
  const [form] = Form.useForm();
  const [bookingForm] = Form.useForm();

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  const showBookingDrawer = () => {
    setBookingDrawer(true);
  };

  const hideBookingDrawer = () => {
    setBookingDrawer(false);
    // @todo: check switch off
  };

  const toggleBookingForm = (checked) => {
    if (checked) {
      showBookingDrawer();
    } else {
      hideBookingDrawer();
    }
    // setShowBookingForm(checked);
  };

  const validateMessages = {
    required: "${label} is required!",
  };

  const onFinish = (values) => {
    const hide = message.loading("Saving..", 0);
    addIssue({
      subject: values.subject,
      project: values.project,
      status: values.status_id,
      assigned: values.assigned_to_id,
      description: values.description,
      priority: values.priority_id,
      tracker: values.tracker_id,
      start_date: values.date[0].format("YYYY-MM-DD"),
      due_date: values?.date[1]?.format("YYYY-MM-DD"),
    }).then(() => {
      hide();
      setVisible(false);
      form.resetFields();
    });
  };

  const onFinishBooking = (values) => {
    setSavingBookingForm(true);
    console.log(values);
    addBooking({
      project_id: values.project,
      ressource_id: values.assigned_to_id,
      activity_id: values.activity_id,
      ratio: values.ratio,
      nb_days: values.nb_days,
      comments: values.comments,
      start_date: values.date[0].format("YYYY-MM-DD"),
      end_date: values.date[1].format("YYYY-MM-DD"),
    })
      .then((response) => {
        setSavingBookingForm(false);

        if (!response.ok) {
          throw response;
        }

        return response.json();
      })
      .then((data) => {
        message.success(`Booking ajouté`);
        hideBookingDrawer();
      })
      .catch((error) => {
        if (typeof error.text !== "function") {
          Modal.error({
            title: "Erreur",
            content: "An error has occurred: Failed to fetch",
          });
          return;
        }
        error.text().then((errorMessage) => {
          const jsonErrors = JSON.parse(errorMessage);
          for (const [key, value] of Object.entries(jsonErrors.errors)) {
            let fieldName = key;
            if (key === "start_date" || key === "end_date") {
              fieldName = "date";
            }

            if (key === "id") {
              Modal.error({
                title: "Erreur",
                content: value,
              });
              continue;
            }

            bookingForm.setFields([
              {
                name: fieldName,
                errors: [value],
              },
            ]);
            // form.setFields({
            //     [fieldName]: {
            //       value: values[fieldName],
            //       errors: [new Error(value)],
            //     },
            //   });
            console.log(`${fieldName}: ${value}`);
          }
        });
      });
  };

  const onFieldsChange = () => {
    const canBook =
      typeof form.getFieldValue("project") !== "undefined" &&
      typeof form.getFieldValue("assigned_to_id") !== "undefined" &&
      typeof form.getFieldValue("date") !== "undefined" &&
      form.getFieldValue("date") !== null &&
      form.getFieldValue("date").length === 2;
    setCanCreateBooking(canBook);

    if (canBook) {
      bookingForm.setFieldsValue({
        project: form.getFieldValue("project"),
        assigned_to_id: form.getFieldValue("assigned_to_id"),
        date: form.getFieldValue("date"),
      });
    }
  };

  return (
    <React.Fragment>
      <AddButton
        type="primary"
        shape="circle"
        icon={
          <PlusCircleOutlined style={{ fontSize: "30px", color: "#fff" }} />
        }
        onClick={showDrawer}
      />
      <Drawer
        title="Nouvelle demande"
        width={"80%"}
        onClose={onClose}
        visible={visible}
        bodyStyle={{ paddingBottom: 80 }}
        footer={
          <div
            style={{
              textAlign: "right",
            }}
          >
            <Button
              htmlType="submit"
              size="large"
              form="new-issue-form"
              shape="round"
              type="primary"
            >
              Créer
              <SendOutlined />
            </Button>
          </div>
        }
      >
        <Form
          form={form}
          layout="vertical"
          id="new-issue-form"
          onFinish={onFinish}
          validateMessages={validateMessages}
          onFieldsChange={onFieldsChange}
          initialValues={{
            status_id: 1,
            tracker_id: 2,
            priority_id: 2,
            assigned_to_id: config.me.id,
            date: [moment()],
          }}
        >
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item
                name="subject"
                label="Sujet"
                rules={[
                  { required: true, message: "Merci de choisir un sujet" },
                ]}
              >
                <Input placeholder="Veuillez saisir le sujet" />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="project"
                label="Projet"
                rules={[
                  { required: true, message: "Merci de choisir un projet" },
                ]}
              >
                <Select
                  placeholder="Choisir un projet"
                  showSearch
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {config.projects.map((project) => (
                    <Option key={project.id} value={project.id}>
                      {project.name}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="assigned_to_id"
                label="Assigné à"
                rules={[
                  { required: true, message: "Merci de choisir un assigné" },
                ]}
              >
                <Select
                  placeholder="Choisir un assigné"
                  showSearch
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {config.users.map((user) => (
                    <Option key={user.id} value={user.id}>
                      {user.fullname}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="status_id"
                label="Statut"
                rules={[
                  { required: true, message: "Merci de choisir un status" },
                ]}
              >
                <Select placeholder="Choisir un status">
                  {config.status.map((status) => (
                    <Option key={status.id} value={status.id}>
                      {status.name}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="priority_id"
                label="Priorité"
                rules={[
                  { required: true, message: "Merci de choisir une priorité" },
                ]}
              >
                <Select placeholder="Choisir une priorité">
                  {config.priorities.map((priority) => (
                    <Option key={priority.id} value={priority.id}>
                      {priority.name}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                name="tracker_id"
                label="Tracker"
                rules={[
                  { required: true, message: "Merci de choisir un tracker" },
                ]}
              >
                <Select placeholder="Choisir un Tracker">
                  {config.trackers.map((tracker) => (
                    <Option key={tracker.id} value={tracker.id}>
                      {tracker.name}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item name="date" label="Date">
                <DatePicker.RangePicker
                  format={DATEFORMAT}
                  style={{ width: "100%" }}
                  getPopupContainer={(trigger) => trigger.parentElement}
                />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item name="description" label="Description">
                <Input.TextArea
                  rows={4}
                  placeholder="Veuillez saisir une description"
                />
              </Form.Item>
            </Col>
          </Row>
          {config.me.admin === true && (
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item>
                  <Tooltip title={canCreateBooking ? '' : 'Merci de choisir un projet, une ressource et une date'}>
                    <Button
                      type="dashed"
                      disabled={!canCreateBooking}
                      size="large"
                      onClick={toggleBookingForm}
                      block
                      icon={<PlusOutlined />}
                    >
                      Créer un booking
                    </Button>
                  </Tooltip>
                </Form.Item>
              </Col>
            </Row>
          )}
        </Form>

        <Drawer
          title="Booking"
          width={"60%"}
          closable={true}
          onClose={hideBookingDrawer}
          visible={bookingDrawer}
          footer={
            <div
              style={{
                textAlign: "right",
              }}
            >
              <Button
                htmlType="submit"
                size="large"
                form="new-booking-form"
                shape="round"
                type="primary"
                loading={savingBookingForm}
              >
                Créer
                <SendOutlined />
              </Button>
            </div>
          }
        >
          <Form
            form={bookingForm}
            layout="vertical"
            id="new-booking-form"
            onFinish={onFinishBooking}
            validateMessages={validateMessages}
            initialValues={{
              project: form.getFieldValue("project"),
              assigned_to_id: form.getFieldValue("assigned_to_id"),
              date: form.getFieldValue("date"),
            }}
          >
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  name="project"
                  label="Projet"
                  rules={[
                    { required: true, message: "Merci de choisir un projet" },
                  ]}
                >
                  <Select
                    placeholder="Choisir un projet"
                    showSearch
                    optionFilterProp="children"
                    filterOption={(input, option) =>
                      option.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    {config.projects.map((project) => (
                      <Option key={project.id} value={project.id}>
                        {project.name}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="assigned_to_id"
                  label="Assigné à"
                  rules={[
                    { required: true, message: "Merci de choisir un assigné" },
                  ]}
                >
                  <Select
                    placeholder="Choisir un assigné"
                    showSearch
                    optionFilterProp="children"
                    filterOption={(input, option) =>
                      option.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    {config.users.map((user) => (
                      <Option key={user.id} value={user.id}>
                        {user.fullname}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item name="date" label="Date">
                  <DatePicker.RangePicker
                    format={DATEFORMAT}
                    style={{ width: "100%" }}
                    getPopupContainer={(trigger) => trigger.parentElement}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  name="activity_id"
                  label="Activité"
                  rules={[
                    {
                      required: true,
                      message: "Merci de choisir une activité",
                    },
                  ]}
                >
                  <Select placeholder="Choisir une Activité">
                    {config.activities.map((activity) => (
                      <Option key={activity.id} value={activity.id}>
                        {activity.name}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item label="Workload">
                  <Input.Group>
                    <Row gutter={8}>
                      <Col span={12}>
                        <Form.Item name="nb_days">
                          <InputNumber
                            style={{ width: "100%" }}
                            placeholder="Jours"
                          />
                        </Form.Item>
                      </Col>
                      <Col span={12}>
                        <Form.Item name="ratio">
                          <InputNumber
                            style={{ width: "100%" }}
                            placeholder="%"
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                  </Input.Group>
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item name="comment" label="Commentaire">
                  <Input.TextArea
                    rows={4}
                    placeholder="Veuillez saisir un commentaire"
                  />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Drawer>
      </Drawer>
    </React.Fragment>
  );
}

export default AddIssue;
