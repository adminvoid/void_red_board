import React from 'react';
import {Drawer, Form, Button, Col, Row, Input, Select, DatePicker, message, Slider, InputNumber} from 'antd';
import {SendOutlined} from '@ant-design/icons';
import moment from 'moment';
import {DATEFORMAT} from '../constants'
import IssuesState, {AppConfig} from "../../state";

const {Option} = Select;

function EditIssue({issue, visible, setVisible}) {
    let {updateIssue} = IssuesState.useContainer()
    let {config} = AppConfig.useContainer()

    const onClose = () => {
        setVisible(false);
    };

    const validateMessages = {
        required: '${label} is required!',
    };

    const initialValues = {
        subject: issue.subject,
        project: issue.project_id,
        status_id: issue.status_id,
        description: issue.description,
        priority_id: issue.priority_id,
        tracker_id: issue.tracker_id,
        done_ratio: issue.done_ratio,
        date: [
            moment(issue.start_date, 'YYYY-MM-DD'),
            issue.due_date && typeof issue.due_date !== 'undefined' ? moment(issue.due_date, 'YYYY-MM-DD') : null,
        ]
    }

    const onFinish = values => {
        let issueToUpdate = {...issue}
        issueToUpdate.subject = values.subject
        issueToUpdate.project_id = values.project
        issueToUpdate.status_id = values.status_id
        issueToUpdate.description = values.description
        issueToUpdate.priority_id = values.priority_id
        issueToUpdate.tracker_id = values.tracker_id
        issueToUpdate.done_ratio = values.done_ratio
        issueToUpdate.start_date = values.date[0].format('YYYY-MM-DD')
        if (values?.date[1] && typeof values?.date[1] !== 'undefined') {
            issueToUpdate.due_date = values?.date[1]?.format('YYYY-MM-DD')
        }

        const hide = message.loading('Saving..', 0);
        updateIssue(issueToUpdate).then(() => {
            hide();
            setVisible(false)
        })
    };

    return (
        <React.Fragment>
            <Drawer
                title="Modifier demande"
                width={'80%'}
                onClose={onClose}
                visible={visible}
                bodyStyle={{paddingBottom: 80}}
                footer={
                    <div style={{
                        textAlign: 'right',
                    }}
                    >
                        <Button htmlType="submit" size="large" form={`edit-new-issue-form-${issue.id}`} shape="round"
                                type="primary">
                            Enregistrer
                            <SendOutlined/>
                        </Button>
                    </div>
                }
            >
                {/*<pre>{JSON.stringify(issue, null, 2)}</pre>*/}
                <Form
                    layout="vertical"
                    id={`edit-new-issue-form-${issue.id}`}
                    onFinish={onFinish}
                    validateMessages={validateMessages}
                    initialValues={initialValues}
                >
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item
                                name="subject"
                                label="Sujet"
                                rules={[{required: true, message: 'Merci de choisir un sujet'}]}
                            >
                                <Input placeholder="Veuillez saisir le sujet"/>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={12}>
                            <Form.Item
                                name="project"
                                label="Projet"
                                rules={[{required: true, message: 'Merci de choisir un projet'}]}
                            >
                                <Select
                                    placeholder="Choisir un projet"
                                    showSearch
                                    optionFilterProp="children"
                                    filterOption={(input, option) =>
                                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                    }
                                >
                                    {config.projects.map((project) => <Option key={project.id}
                                                                              value={project.id}>{project.name}</Option>)}
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <Form.Item
                                name="done_ratio"
                                label="% Réalisé"
                            >
                                <Slider
                                    min={0}
                                    max={100}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={12}>
                            <Form.Item
                                name="status_id"
                                label="Statut"
                                rules={[{required: true, message: 'Merci de choisir un status'}]}
                            >
                                <Select placeholder="Choisir un status">
                                    {config.status.map((status) => <Option key={status.id}
                                                                           value={status.id}>{status.name}</Option>)}
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <Form.Item
                                name="priority_id"
                                label="Priorité"
                                rules={[{required: true, message: 'Merci de choisir une priorité'}]}
                            >
                                <Select placeholder="Choisir une priorité">
                                    {config.priorities.map((priority) => <Option key={priority.id}
                                                                                 value={priority.id}>{priority.name}</Option>)}
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={12}>
                            <Form.Item
                                name="tracker_id"
                                label="Tracker"
                                rules={[{required: true, message: 'Merci de choisir un tracker'}]}
                            >
                                <Select placeholder="Choisir un Tracker">
                                    {config.trackers.map((tracker) => <Option key={tracker.id}
                                                                              value={tracker.id}>{tracker.name}</Option>)}
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <Form.Item
                                name="date"
                                label="Date"
                            >
                                <DatePicker.RangePicker
                                    format={DATEFORMAT}
                                    style={{width: '100%'}}
                                    getPopupContainer={trigger => trigger.parentElement}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item
                                name="description"
                                label="Description"
                            >
                                <Input.TextArea rows={4} placeholder="Veuillez saisir une description"/>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Drawer>

        </React.Fragment>
    );
}

export default EditIssue;
