import React, {useState} from 'react';
import styled from '@emotion/styled';
import {colors} from '@atlaskit/theme';
import {borderRadius, grid} from '../constants';
import {Tag, Button, Card, Dropdown, Menu, message, Popconfirm, Progress} from 'antd';
import IssuesState, {AppConfig} from "../../state";
import {CheckOutlined, CloseOutlined, EditOutlined, EllipsisOutlined} from '@ant-design/icons';
import EditIssue from '../issue/edit'

const getBackgroundColor = (
    isDragging,
    isGroupedOver,
) => {
    if (isDragging) {
        return colors.Y50;
    }

    if (isGroupedOver) {
        return colors.N30;
    }

    return colors.N0;
};

const getBorderColor = (isDragging) =>
    isDragging ? colors.N400A : 'transparent';

const imageSize = 40;


const Container = styled.a`
  border-radius: ${borderRadius}px;
  border: 2px solid transparent;
  border-color: ${(props) => getBorderColor(props.isDragging, props.colors)};
  background-color: ${(props) =>
    getBackgroundColor(props.isDragging, props.isGroupedOver, props.colors)};
  box-shadow: ${({isDragging}) =>
    isDragging ? `2px 2px 1px ${colors.N70}` : 'none'};
  box-sizing: border-box;
  padding: 0;
  min-height: ${imageSize}px;
  margin-bottom: ${grid}px;
  user-select: none;

  /* anchor overrides */
  color: ${colors.N900};

  &:hover,
  &:active {
    color: ${colors.N900};
    text-decoration: none;
  }

  &:focus {
    outline: none;
    border-color: ${colors.N400A};
    box-shadow: none;
  }

  /* flexbox */
  display: flex;
  
  .ant-card-body, .ant-card-head {
    padding: ${grid}px;
  }
  
  .ant-card-head {
    min-height: initial;
    border: none;
    padding-bottom: 0;
    }
    
    
 
  .ant-card-head-title {
    padding: 0;
    display:flex;
        justify-content: space-between;
        
        .anticon {
        cursor: pointer;
            transition: color .3s;
            
            &:hover {
            color: #1890ff; 
            }
        }
 }
`;

const Avatar = styled.img`
  width: ${imageSize}px;
  height: ${imageSize}px;
  border-radius: 50%;
  margin-right: ${grid}px;
  flex-shrink: 0;
  flex-grow: 0;
`;

const Content = styled.div`
  /* flex child */
  flex-grow: 1;

  /*
    Needed to wrap text in ie11
    https://stackoverflow.com/questions/35111090/why-ie11-doesnt-wrap-the-text-in-flexbox
  */
  flex-basis: 100%;

  /* flex parent */
  display: flex;
  flex-direction: column;
`;

const BlockQuote = styled.div`
  &::before {
    content: open-quote;
  }

  &::after {
    content: close-quote;
  }
`;

const Footer = styled.div`
  display: flex;
  margin-top: ${grid}px;
  align-items: center;
  flex-wrap: wrap;
`;

const Author = styled.small`
  color: ${colors.N400A};
  flex-grow: 0;
  margin: 0;
  background-color: ${colors.Y50};
  border-radius: ${borderRadius}px;
  font-weight: normal;
  padding: ${grid / 2}px;
`;

const QuoteId = styled.small`
  flex-grow: 1;
  flex-shrink: 1;
  margin: 0;
  font-weight: normal;
  text-overflow: ellipsis;
  text-align: right;
`;

function getStyle(provided, style) {
    if (!style) {
        return provided.draggableProps.style;
    }

    return {
        ...provided.draggableProps.style,
        ...style,
    };
}

const priorityColor = {
    1: '#f1f1f1', // Low
    2: 'default', // Normal
    3: '#BC5090', // High
    4: '#D2D462', // Urgent
    5: '#FF6361' // Immediate
};

const statusColor = {
    1: 'default', // Nouveau
    2: '#ebc67e', // En cours
    3: '#52c41c', // Terminé
    4: '#FF6361', // En attente
    5: 'default', // Fermé
    6: 'default', // Rejeté
    7: '#afa3c4', // Prêt recette
    8: '#4e97d6', // Recette OK
    9: '#fa3838', // Recette KO
};


const QuoteTitle = ({quote}) => {
    const [visibleDrop, setVisibleDrop] = React.useState(false);
    const [visibleConfirmClose, setVisibleConfirmClose] = React.useState(false);
    const [visibleConfirmDone, setVisibleConfirmDone] = React.useState(false);
    const [visibleEditForm, setVisibleEditForm] = React.useState(false);

    let {markIssueAsClose, markIssueAsDone} = IssuesState.useContainer()

    const handleMenuClick = (e) => {
        if (e.key === 'edit') {
            setVisibleDrop(false)
            setVisibleEditForm(true)
        }

        if (e.key === 'close') {
            setVisibleConfirmClose(true)
        }

        if (e.key === 'done') {
            setVisibleConfirmDone(true)
        }

    }

    const confirmClose = (e) => {
        e.stopPropagation()
        quote.status_id = 5 // closed.
        const hide = message.loading('Saving..', 0);
        setVisibleConfirmClose(false)
        setVisibleDrop(false)
        markIssueAsClose(quote).then(() => {
            hide();
            message.success(`"${quote.subject}" fermé`);
        })
    }

    const confirmDone = (e) => {
        e.stopPropagation()
        quote.status_id = 3 // done.
        quote.done_ratio = 100 // done.
        const hide = message.loading('Saving..', 0);
        setVisibleConfirmDone(false)
        setVisibleDrop(false)
        markIssueAsDone(quote).then(() => {
            hide();
            message.success(`"${quote.subject}" terminé`);
        })
    }

    const cancelClose = (e) => {
        e.stopPropagation()
        setVisibleConfirmClose(false)
        setVisibleDrop(false)
    }

    const cancelDone = (e) => {
        e.stopPropagation()
        setVisibleConfirmDone(false)
        setVisibleDrop(false)
    }

    const handleDropVisibleChange = (flag) => {
        setVisibleDrop(flag)
    }

    const menu = (
        <Menu
            onClick={handleMenuClick}
        >
            <Menu.Item key="edit" icon={<EditOutlined/>}>
                Modifier
            </Menu.Item>
            <Menu.Item key="done" icon={<CheckOutlined/>}>
                <Popconfirm
                    title="Êtes-vous sûr de terminer cette tâche?"
                    visible={visibleConfirmDone}
                    onConfirm={confirmDone}
                    onCancel={cancelDone}
                    okText="Terminer"
                    cancelText="Annuler"
                >
                    <a href="#">Terminer</a>
                </Popconfirm>
            </Menu.Item>
            <Menu.Item key="close" icon={<CloseOutlined/>}>
                <Popconfirm
                    title="Êtes-vous sûr de fermer cette tâche?"
                    visible={visibleConfirmClose}
                    onConfirm={confirmClose}
                    onCancel={cancelClose}
                    okText="Fermé"
                    cancelText="Annuler"
                >
                    <a href="#">Fermé</a>
                </Popconfirm>
            </Menu.Item>
        </Menu>
    );

    return <React.Fragment>
        <Tag color="lime" style={{
            maxWidth: '85%',
            overflow: 'hidden',
            textOverflow: 'ellipsis'
        }}>{quote.project_name}</Tag>
        <Dropdown onVisibleChange={handleDropVisibleChange}
                  visible={visibleDrop}
                  overlay={menu}
                  trigger={['click']}>
            <Button size="small" shape="circle">
                <EllipsisOutlined key="ellipsis"/>
            </Button>
        </Dropdown>
        <EditIssue issue={quote} visible={visibleEditForm} setVisible={(flag) => setVisibleEditForm(flag)} />
    </React.Fragment>
}


// Previously this extended React.Component
// That was a good thing, because using React.PureComponent can hide
// issues with the selectors. However, moving it over does can considerable
// performance improvements when reordering big lists (400ms => 200ms)
// Need to be super sure we are not relying on PureComponent here for
// things we should be doing in the selector as we do not know if consumers
// will be using PureComponent
function QuoteItem(props) {
    const {
        quote,
        isDragging,
        isGroupedOver,
        provided,
        style,
        isClone,
        index,
    } = props;

    let {config} = AppConfig.useContainer()
    const {status, trackers, priorities, app} = config

    const ideaStatus = status.find(status => status.id === quote.status_id)
    const ideaTracker = trackers.find(tracker => tracker.id === quote.tracker_id)
    const ideaPriority = priorities.find(priority => priority.id === quote.priority_id)

    return (
        <Container
            href={`${app.issues_url}${quote.id}`}
            target="_blank"
            isDragging={isDragging}
            isGroupedOver={isGroupedOver}
            isClone={isClone}
            // colors={quote.author.colors}
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            style={getStyle(provided, style)}
            data-is-dragging={isDragging}
            data-testid={quote.id}
            data-index={index}
            aria-label={`${quote.subject}`}
        >
            <Card
                title={<QuoteTitle quote={quote}/>}
                bordered={false}
                style={{width: '100%'}}
                // actions={[
                //     <SettingOutlined key="setting" />,
                //     <EditOutlined key="edit" />,
                //     <EllipsisOutlined key="ellipsis" />,
                // ]}
            >
                <Progress percent={quote.done_ratio} size="small" />
                <BlockQuote>{quote.subject}</BlockQuote>
                <Footer>
                    <Tag color="default" style={{marginBottom: 8}}>{ideaTracker.name}</Tag>
                    <Tag color={statusColor[quote.status_id]} style={{marginBottom: 8}}>{ideaStatus.name}</Tag>
                    <Tag color={priorityColor[quote.priority_id]} style={{marginBottom: 8}}>{ideaPriority.name}</Tag>
                    {/* <QuoteId><a href={`${app.issues_url}{quote.id}`} target="_blank">id:{quote.id}</a></QuoteId> */}
                </Footer>
            </Card>

            {/*<Avatar src={quote.author.avatarUrl} alt={quote.author.name}/>*/}
            {/*{isClone ? <CloneBadge>Clone</CloneBadge> : null}*/}
            {/*<Content>*/}
            {/*    <Tag color="default" style={{marginBottom: 8}}>{quote.project_name}</Tag>*/}
            {/*    <BlockQuote>{quote.subject}</BlockQuote>*/}

            {/*</Content>*/}
        </Container>
    );
}

export default React.memo(QuoteItem);
