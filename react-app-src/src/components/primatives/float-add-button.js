import styled from '@emotion/styled';
import {Button} from 'antd';

export default styled(Button)`
  position: fixed !important;
  left: 0 !important;
  bottom: 20px !important;
  right: 0 !important;
  z-index: 999 !important;
  user-select: none !important;
  border-radius: 100% !important;
  cursor: pointer !important;
  margin-right: auto !important;
  margin-left: auto !important;
  width: 76px !important;
  height: 76px !important;
  box-shadow: 0 15px 20px rgba(0,0,0,.15) !important;
`;
