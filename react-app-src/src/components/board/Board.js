import React from 'react';
import styled from '@emotion/styled';
import {Global, css} from '@emotion/core';
import {colors} from '@atlaskit/theme';
import AddIssue from '../issue/add';
import Column from './Column';
import {reorderQuoteMap} from '../reorder';
import {DragDropContext, Droppable} from 'react-beautiful-dnd';
import IssuesState, {AppConfig} from '../../state'
import {message, Badge} from 'antd';
import {useQueryCache} from "react-query";

const ParentContainer = styled.div`
  height: ${({height}) => height};
  overflow-x: hidden;
  overflow-y: auto;
`;

const Container = styled.div`
  background-color: ${colors.DN40};
  min-height: calc(100vh - 20px);
  /* like display:flex but will allow bleeding over the window width */
  min-width: 100vw;
  display: inline-flex;
`;

const ColumnTitle = ({title, total}) => {
    return <div>
        <span>{title}</span>
    </div>
}

const Board = (props) => {
    let {issues, setIssues, moveIssue} = IssuesState.useContainer()
    let {config, setConfig} = AppConfig.useContainer()
    const authors = config.users.map(user => user.fullname)
    const {
        containerHeight,
        withScrollableColumns,
        refetch
    } = props;

    React.useEffect(() => {
        const handleP2PCallback = (e) => {
            console.log(e)
            refetch().then(data => {
                setIssues(data.issues)
            })
            //cache.invalidateQueries('ideas')
        }
        window.addEventListener('p2p.data', handleP2PCallback);

        return () => {
            window.removeEventListener('p2p.data', handleP2PCallback);
        };
    }, []);

    const onDragEnd = (result) => {
        // dropped nowhere
        if (!result.destination) {
            return;
        }

        const source = result.source;
        const destination = result.destination;

        // did not move anywhere - can bail early
        if (
            source.droppableId === destination.droppableId &&
            source.index === destination.index
        ) {
            return;
        }

        let issueToUpdate;
        if (source.droppableId !== destination.droppableId) {
            issueToUpdate = {...issues[source.droppableId][source.index]};
            const targetUser = config.users.find(user => user.fullname === destination.droppableId)
            issueToUpdate.assigned_to_id = targetUser.id
        }

        const data = reorderQuoteMap({
            quoteMap: issues,
            source,
            destination,
        });

        setIssues(data.quoteMap)

        // Move to another assinge.
        if (moveIssue && source.droppableId !== destination.droppableId) {
            const hide = message.loading('Saving..', 0);
            moveIssue({issue: issueToUpdate, issuesMap: data.quoteMap}).then(() => hide());
        }
    };

    const board = (
        <Droppable
            droppableId="board"
            type="COLUMN"
            direction="horizontal"
            ignoreContainerClipping={Boolean(containerHeight)}
        >
            {(provided) => (
                <Container ref={provided.innerRef} {...provided.droppableProps}>
                    {authors.map((column, index) => {
                        const columnIssues = issues[column] || [];
                        return (
                            <Column
                                key={column}
                                index={index}
                                title={<ColumnTitle title={column} total={columnIssues.length} />}
                                id={column}
                                quotes={columnIssues}
                                isScrollable={withScrollableColumns}
                            />
                        )
                    })}
                    {provided.placeholder}
                </Container>
            )}
        </Droppable>
    );

    return (
        <React.Fragment>
            <DragDropContext onDragEnd={onDragEnd}>
                {containerHeight ? (
                    <ParentContainer height={containerHeight}>{board}</ParentContainer>
                ) : (
                    board
                )}
            </DragDropContext>
            <Global
                styles={css`
            .redBoardApp {
              background: ${colors.DN40};
            }
          `}
            />
            <AddIssue/>
        </React.Fragment>
    );
}

export default Board
