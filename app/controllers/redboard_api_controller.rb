class RedboardApiController < ApplicationController
  accept_api_auth :index, :create, :update, :createBooking
  before_action :global_authorize, :init_pusher
  before_filter :cors_set_access_control_headers
  skip_before_action :verify_authenticity_token

  def cors_set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
    headers['Access-Control-Request-Method'] = '*'
    headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Redmine-API-Key, x-redmine-api-key'
  end

  def options
    render nothing: true
  end

  def index
    #raise @current_user.inspect
    @groupd_id = Setting.plugin_void_red_board['group'].to_i
    if(params.has_key?(:group))
      @groupd_id =  params['group'].to_i
    end
    principal = Principal.find(User.current.id)
    @trackers = Tracker.all.select(:id, :name)
    @projects_raw = Project.active.all.select(:id, :name)
    @priorities = IssuePriority.all.select(:id, :name)
    @status = IssueStatus.all.select(:id, :name)
    @users_raw = User.where(type: "User").where(status: 1).select(:id, :login, :firstname, :lastname, "CONCAT(users.firstname, ' ', users.lastname) as fullname",)
    @users = []
    @users_raw.each {|user|
        if user.group_ids.include?(@groupd_id)
          @users.push(user)
        end
    }
    @projects = []
    @projects_raw.each {|p|
        if principal.member_of?(p) || User.current.admin
          @projects.push(p)
        end
    }

    @issues = Issue
          .joins(:custom_values)
          .joins(:assigned_to)
          .joins(:project)
          .where(is_private: false)
          .where.not(status: Setting.plugin_void_red_board['status'])
          .where(:custom_values => {
            :custom_field_id => Setting.plugin_void_red_board['idea_custom_field'].to_i,
            :value => 1,
          })
        .select(
          :id,
          :tracker_id,
          :project_id,
          :subject,
          :description,
          :due_date,
          :status_id,
          :assigned_to_id,
          :priority_id,
          :author_id,
          :created_on,
          :updated_on,
          :start_date,
          :done_ratio,
          :estimated_hours,
          'projects.name as project_name',
          "CONCAT(users.firstname, ' ', users.lastname) as assigned_to_name",
          "CAST(issues.id as CHAR) as idea_id"
        )

    render :json => {
      :app => {
        issues_url: url_for(controller: 'issues', action: 'index', trailing_slash: true)
      },
      :me => {
        id: @current_user.id,
        login: @current_user.login,
        firstname: @current_user.firstname,
        lastname: @current_user.lastname,
        admin: @current_user.admin
      },
      :issues => @issues.group_by {|issue| issue.assigned_to_name},
      :trackers => @trackers,
      :projects => @projects,
      :priorities => @priorities,
      :status => @status,
      :activities => TimeEntryActivity.where(active: 1).select(:id, :name),
      :users => @users
    }
  end

  def create
    @issue = Issue.new()
    @issue.init_journal(User.current)
    @issue.author ||= User.current
    @issue.subject = params[:subject]
    @issue.description = params[:description]
    @issue.project_id = params[:project].to_i
    @issue.assigned_to_id = params[:assigned].to_i
    @issue.tracker_id = params[:tracker].to_i
    @issue.priority_id = params[:priority].to_i
    @issue.status_id = params[:status].to_i
    @issue.custom_field_values = {Setting.plugin_void_red_board['idea_custom_field'].to_i => 1}
    #attrs = {}.deep_dup
    #attrs[:tracker_id] = params[:tracker].to_i
    #attrs[:status_id] = params[:status].to_i
    #attrs[:priority_id] = params[:priority].to_i
    #@issue.safe_attributes = attrs

    if params[:start_date]
      @issue.start_date ||= Date.parse(params[:start_date])
    end
    if params[:due_date]
      @issue.due_date ||= Date.parse(params[:due_date])
    end


    if @issue.save(validate: false)
      userAssigne = User.find(params[:assigned].to_i)
      # Add member if not already
      principalAssigned = Principal.find(params[:assigned].to_i)
      issueProject = Project.find(@issue.project.id);
      if !principalAssigned.member_of?(issueProject) && !userAssigne.admin?
          memberRoles = Setting.plugin_void_red_board['default_roles'].map { |x| Role.find(x.to_i) }
          member = Member.new(:project => issueProject, :principal => principalAssigned, :roles => memberRoles)
          issueProject.members << member
          issueProject.save!
      end

      # Update issue status workflow.
      #@updatedIssue = Issue.find(@issue.id)
      #@updatedIssue.tracker_id = params[:tracker].to_i
      #@updatedIssue.priority_id = params[:priority].to_i
      #@updatedIssue.status_id = params[:status].to_i
      #@updatedIssue.save(validate: false)!

      hashIssue = @issue.as_json
      hashIssue['assigned'] = userAssigne.name
      hashIssue['assigned_to_name'] = userAssigne.name
      hashIssue['idea_id'] = @issue.id.to_s
      hashIssue['project_name'] = issueProject.name

      @pusher.trigger('redmine', 'idea-event', {:type => 'create', :message => @issue.subject})

      render json: hashIssue
    else
      render :json => {
        :id => "error"
      }
    end
  end

  def update

    @issue = Issue.find(params[:id])

    old_status_id = @issue.status_id
    old_assigned_to_id = @issue.assigned_to_id
    old_priority_id = @issue.priority_id
    old_tracker_id = @issue.tracker_id
    old_subject = @issue.subject
    old_project_id = @issue.project_id
    old_start_date = @issue.start_date
    old_due_date = @issue.due_date

    @issue.subject = params[:subject]
    @issue.description = params[:description]
    @issue.project_id = params[:project_id].to_i
    @issue.assigned_to_id = params[:assigned_to_id].to_i
    @issue.tracker_id = params[:tracker_id].to_i
    @issue.priority_id = params[:priority_id].to_i
    @issue.status_id = params[:status_id].to_i
    @issue.done_ratio = params[:done_ratio].to_i
    if params[:start_date]
      @issue.start_date = Date.parse(params[:start_date])
    end
    if params[:due_date]
      @issue.due_date = Date.parse(params[:due_date])
    end

    if @issue.save!

      # Create and Add a journal
      note = Journal.new(:journalized => @issue, user: User.current)
      foundDiff = false
      if old_status_id != @issue.status_id
        note.details << JournalDetail.new(:property => 'attr', :prop_key => 'status_id', :old_value => old_status_id,:value => @issue.status_id)
        foundDiff = true
      end
      if old_assigned_to_id != @issue.assigned_to_id
        note.details << JournalDetail.new(:property => 'attr', :prop_key => 'assigned_to_id', :old_value => old_assigned_to_id,:value => @issue.assigned_to_id)
        foundDiff = true
      end
      if old_priority_id != @issue.priority_id
        note.details << JournalDetail.new(:property => 'attr', :prop_key => 'priority_id', :old_value => old_priority_id,:value => @issue.priority_id)
        foundDiff = true
      end
      if old_tracker_id != @issue.tracker_id
        note.details << JournalDetail.new(:property => 'attr', :prop_key => 'tracker_id', :old_value => old_tracker_id,:value => @issue.tracker_id)
        foundDiff = true
      end

      if old_subject != @issue.subject
        note.details << JournalDetail.new(:property => 'attr', :prop_key => 'subject', :old_value => old_subject,:value => @issue.subject)
        foundDiff = true
      end

      if old_project_id != @issue.project_id
        note.details << JournalDetail.new(:property => 'attr', :prop_key => 'project_id', :old_value => old_project_id,:value => @issue.project_id)
        foundDiff = true
      end

      if old_start_date != @issue.start_date
        note.details << JournalDetail.new(:property => 'attr', :prop_key => 'start_date', :old_value => old_start_date,:value => @issue.start_date)
        foundDiff = true
      end

      if old_due_date != @issue.due_date
        note.details << JournalDetail.new(:property => 'attr', :prop_key => 'due_date', :old_value => old_due_date,:value => @issue.due_date)
        foundDiff = true
      end

      if foundDiff
        note.save!
      end

      # Add member if not already
      userAssigne = User.find(params[:assigned_to_id].to_i)
      principalAssigned = Principal.find(params[:assigned_to_id].to_i)
      issueProject = Project.find(@issue.project.id);
      if !principalAssigned.member_of?(issueProject) && !userAssigne.admin?
          memberRoles = Setting.plugin_void_red_board['default_roles'].map { |x| Role.find(x.to_i) }
          member = Member.new(:project => issueProject, :principal => principalAssigned, :roles => memberRoles)
          issueProject.members << member
          issueProject.save!
      end

      hashIssue = @issue.as_json
      hashIssue['assigned'] = userAssigne.name
      hashIssue['assigned_to_name'] = userAssigne.name
      hashIssue['idea_id'] = @issue.id.to_s
      hashIssue['project_name'] = issueProject.name

     @pusher.trigger('redmine', 'idea-event', {:type => 'update', :message => @issue.subject})
      render json: hashIssue
    else
      render :json => {
        :id => "error"
      }
    end
  end

  def createBooking
    @booking_ressource = BelsonBookingRessource.new
    @booking_ressource.project = Project.find(params[:project_id])
    @booking_ressource.author = @current_user
    @booking_ressource.author_id = @current_user.id
    @booking_ressource.start_date = params[:start_date]
    @booking_ressource.end_date = params[:end_date]
    @booking_ressource.ressource_id = params[:ressource_id]
    @booking_ressource.project_id = params[:project_id]
    @booking_ressource.activity_id = params[:activity_id]
    @booking_ressource.ratio = params[:ratio]
    @booking_ressource.nb_days = params[:nb_days]
    @booking_ressource.comments = params[:comments]

    if @booking_ressource.save
      render json: @booking_ressource
    else
      response.status = 422
      render :json => {
        :errors => @booking_ressource.errors.as_json(full_messages: true)
      }
    end
  end

  #
  # User logged in
  #
  def set_user
    @current_user ||= User.current
  end

  def init_pusher
    @pusher = Pusher::Client.new(
        app_id: Setting.plugin_void_red_board['pusher_app_id'],
        key: Setting.plugin_void_red_board['pusher_key'],
        secret: Setting.plugin_void_red_board['pusher_secret'],
        cluster: Setting.plugin_void_red_board['pusher_cluster']
    )
  end

  #
  # Need Login
  #
  def global_authorize
    set_user
    render_403 unless @current_user.type == 'User'
  end

end
