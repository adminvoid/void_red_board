class RedboardController < ApplicationController
  before_action :global_authorize

  def index
    @groups = Group.givable.sorted
  end

  def view
    @group_id = params[:id];
    @pusher_app_key = Setting.plugin_void_red_board['pusher_key']
    @pusher_cluster = Setting.plugin_void_red_board['pusher_cluster']
  end

  #
  # User logged in
  #
  def set_user
    @current_user ||= User.current
  end

  #
  # Need Login
  #
  def global_authorize
    set_user
    render_403 unless @current_user.type == 'User'
  end
end
